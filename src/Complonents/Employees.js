import {useState, useEffect} from "react";
import Employee from "./Employee";
import EmployeePopup from "./EmployeePopup";
import ErrorBoundary from "./Common/ErrorBoundary";
import addEmployees from "../Redux/Actions/addEmployee"
import {connect} from 'react-redux';
import { toast } from "react-toastify";

function Employees(props){
    
    const [employees, setEmployees] = useState([]);
    const [openPopup, setOpenPopupFlag] = useState(false);
    const [editEmployee, setEditEmployee] = useState(false);
    const [employee, setEmployee] = useState({Id:'',Name:'',Location:'',Salary:''});
    const [errors, setErrors] = useState({});
    const listElements = employees.map((emp) =>
            <Employee key={emp.Id}  data={emp} deleteEmployee={deleteEmployee} setEidtPopup={setEidtPopup}/>
        );
    const LOCAL_STORAGE_KEY = 'employeesApp.employees';

    useEffect(() => {
       const storedEmployees = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
       if(storedEmployees) setEmployees(storedEmployees);
    }, []);
    
    useEffect(() => {
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(employees));
    }, [employees]);

    function deleteEmployee(ID)
    {
        if(window.confirm("Are you sure want to delete this employee?")){
            const deletedEmployee = employees.filter(function( obj ) {
                return obj.Id !== ID;
            });
            setEmployees(deletedEmployee);
        }
    }

    function handleChange({target})
    {
        if(target.name === 'Id' || target.name === 'Salary')
        {
            setEmployee({
                ...employee,[target.name]: parseInt(target.value)
            });
        }
        else{
            setEmployee({
                ...employee, [target.name]: target.value
            });
        }
        
    }

    function formIsValid()
    {
        const _errors = {};
        const data = employee;
        if(!data.Id || data.Id === "") _errors.Id = "EmployeeID is required";
        if(!data.Name || data.Name === "") _errors.Name = "Name is required";
        if(!data.Location || data.Location === "") _errors.Location = "Location is required";
        if(!data.Salary || data.Salary === "") _errors.Salary = "Salary is required";

        setErrors(_errors);
        return Object.keys(_errors).length === 0;
    }


    function handleSubmit(event) 
    {
        event.preventDefault();
        if(!formIsValid()) return;
        setClosePopup();
        const addEmployee = employee;
        if(employees.find(obj=>obj["Id"] === parseInt(addEmployee.Id))){
            let updateEmployees = [];
            if(editEmployee === false){
                if(window.confirm("Employee with same Id already exists, Do you want to update it?")){
                    updateEmployees = employees.map(obj =>
                        obj.Id === parseInt(addEmployee.Id) 
                        ? { ...obj, Name: addEmployee.Name, Location: addEmployee.Location, Salary: addEmployee.Salary}
                        :obj
                    );
                }
                else updateEmployees = employees
            }
            else{
                updateEmployees = employees.map(obj =>
                    obj.Id === parseInt(addEmployee.Id) 
                    ? { ...obj, Name: addEmployee.Name, Location: addEmployee.Location, Salary: addEmployee.Salary}
                    :obj
                );
            }
            setEmployees(updateEmployees);
            toast.success("Employee updated.");
        }
        else{
            setEmployees(prevEmployees => {
                return [...prevEmployees, {Id: addEmployee.Id, Name: addEmployee.Name, Location: addEmployee.Location, Salary: addEmployee.Salary}]
            });
            toast.success("Employee added.");
        }
        //props.dispatch(addEmployees(updatedEmployees));
    }
    
    function setOpenPopup()
    {
        setOpenPopupFlag(true);
        setEditEmployee(false);
        setEmployee({Id:undefined,Name:'',Location:'',Salary:undefined});
    }

    function setClosePopup()
    {
        setOpenPopupFlag(false);
        setErrors({});
    }

    function setEidtPopup(data)
    {
        setOpenPopupFlag(true);
        setEditEmployee(true);
        setEmployee(data);
    }
    return (
        <div>
            <ErrorBoundary>
            <h4 style={{display:"flex"}}>Welcome, <b>{props.user.length > 0  ? props.user.at(-1).Username : 'Stranger'}</b></h4>  
            <h2>Employee Portal</h2>
            <button type="submit" className="btn btn-primary" onClick={()=> setOpenPopup()}>Add Employee</button>
            <h4>Employees Count: {employees.length}</h4>
            {listElements}
            <EmployeePopup 
            openPopup={openPopup} 
            editEmployee={editEmployee}
            handleSubmit={handleSubmit} 
            data={employee}
            onChange={handleChange}
            setClosePopup={setClosePopup}
            errors={errors}
            >
            </EmployeePopup>
            </ErrorBoundary>
        </div>
        );
    }

function mapsStateToProps(state){
    return{
        user: state.user,
        storeEmployees: state.storeEmployees
    }
}

export default connect(mapsStateToProps)(Employees);