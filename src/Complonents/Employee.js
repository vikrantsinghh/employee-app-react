function Employee(props){
    return (
        <div style={{border:"3px solid red"}}>
            <div>
                <p>Employee ID : <b>{props.data.Id}</b></p>
                <p>Employee Name : <b>{props.data.Name}</b></p>
                <p>Employee Location : <b>{props.data.Location}</b></p>
                <p>Employee Salary : <b>{props.data.Salary}</b></p>
            </div>
            <div>
                <button type="submit" className="btn btn-danger" onClick={() => props.deleteEmployee(props.data.Id)}>Delete</button>&nbsp;
                <button type="submit" className="btn btn-primary" onClick={() => props.setEidtPopup(props.data)}>Update</button>
            </div>
        </div>
    );
}

export default Employee;