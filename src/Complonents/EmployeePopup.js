import {Dialog, DialogTitle, DialogContent} from '@material-ui/core'
import TextInput from './Common/TextInput';

function EmployeePopup(props){
    const {openPopup, handleSubmit, data, onChange, editEmployee, setClosePopup, errors} = props;

    return (
        <Dialog open={openPopup} maxWidth="md">
            <DialogTitle style={{border:"2px solid red"}}>
                Add an Employee
                <div style={{float: 'right'}}>
                    <button type="button" className="close" aria-label="Close" onClick={() => setClosePopup()}><span aria-hidden="true">&times;</span></button>
                </div>
            </DialogTitle>
            <DialogContent>
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <TextInput 
                        id="EmployeeID"
                        name="Id"
                        label="Employee ID"
                        type="number"
                        onChange={onChange}
                        value={data.Id}
                        disabled={editEmployee}
                        error={errors.Id}
                    />
                </div>
                <div className="form-group">
                    <TextInput 
                        id="EmployeeName"
                        name="Name"
                        label="Employee Name"
                        type="text"
                        onChange={onChange}
                        value={data.Name}
                        error={errors.Name}
                    />
                </div>
                <div className="form-group">
                    <TextInput 
                        id="EmployeeLocation"
                        name="Location"
                        label="Employee Location"
                        type="text"
                        onChange={onChange}
                        value={data.Location}
                        error={errors.Location}
                    />
                </div>
                <div className="form-group">
                    <TextInput 
                        id="EmployeeSalary"
                        name="Salary"
                        label="Employee Salary"
                        type="number"
                        onChange={onChange}
                        value={data.Salary}
                        error={errors.Salary}
                    />
                </div>
                <div className="form-group">
                    <button type='submit' className="btn btn-primary">{editEmployee? 'Update' : 'Save'}</button>
                </div>
            </form>
            </DialogContent>
        </Dialog>
    );
}

export default EmployeePopup;