import {Link} from 'react-router-dom'
import '../../Header.css';

function Header(props){
    return (
        <ul>
          <li>
            <Link to="/" className='link'>Employees</Link>
          </li>
          <li>
            <Link to="/login" className='link'>Login</Link>
          </li>
          <li>
            <Link to="/about" className='link'>About</Link>
          </li>
        </ul>
    );
}

export default Header;