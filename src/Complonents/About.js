import {Link} from 'react-router-dom'

function About(props){
    return (
        <>
            <h1>React Training Hands On Application</h1>
            <p>
                <Link to="/" className='btn btn-primary'>Back to Employees</Link>
            </p>
        </>
    );
}

export default About;