import React, { useState } from 'react';
import { Navigate  } from 'react-router-dom';
import TextInput from './Common/TextInput';
import {connect} from 'react-redux';
import loginUser from '../Redux/Actions/loginUser'

function Login(props){
    
    const [user, setUser] = useState({
        Username:'',
        Password:''
    });
    const [errors, setErrors]= useState({
        Username:'',
        Password:''
    });

    const [isLoggedIn, setIsLoggedIn] = useState(false);
    
    function handleChange({target}){
        //console.log(target.value);
        setUser({
                ...user,
                [target.name]: target.value
            }
        );
        
    }

    function formIsValid(){
        const _errors = {};
        if(!user.Username || user.Username === "") _errors.Username = "Username is required";
        if(!user.Password || user.Password === "") _errors.Password = "Password is required";

        setErrors(_errors);
        return Object.keys(_errors).length === 0;
    }
    
    function handleSubmit(event) {
        event.preventDefault();
        if(!formIsValid()) return;
        props.dispatch(loginUser(user));
        setIsLoggedIn(true);
        //console.log("Login details:ID "+ user.Username+" Password:"+ user.Password);
    }

    if (isLoggedIn) {
        return <Navigate to='/' />
       }

    return (
    <div className="container">
        <div className="row">
            <div className="col-md-6 col-md-offset-6">
                <h1>Login Page</h1>
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                    <TextInput 
                                id="UserId"
                                name="Username"
                                label="Username"
                                type="text"
                                onChange={handleChange}
                                value={user.Username}
                                error={errors.Username}
                            />
                    </div>
                    <div className="form-group">
                        <TextInput 
                                id="Password"
                                name="Password"
                                label="Password"
                                type="password"
                                onChange={handleChange}
                                value={user.Password}
                                error={errors.Password}
                            />
                    </div>
                    <div className="form-group">
                            <button type='submit' className="btn btn-primary">Login</button>
                        </div>
                </form>
                </div>
            </div>
        </div>
    );
}

function mapsStateToProps(state){
    return{
        user: state.user
    }
}

export default connect(mapsStateToProps)(Login);