import {Link} from 'react-router-dom'

function NoPage(props){
    return (
        <>
            <h1>Page does not exist!</h1>
            <p>
                <Link to="/" className='btn btn-primary'>Back to Home</Link>
            </p>
        </>
        
    );
}

export default NoPage;