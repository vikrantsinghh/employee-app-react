import './App.css';
import Employees from './Complonents/Employees';
import About from './Complonents/About';
import Login from './Complonents/Login';
import NoPage from './Complonents/NoPage';
import Header from './Complonents/Common/Header';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Route, Routes} from 'react-router-dom'
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <div className="App">
      <div className="container h-100">
      <ToastContainer autoClose={3000} hideProgressBar />
      <Header />
      <Routes>
        <Route path="/" exact element={<Employees />} />
        <Route path="/login" element={<Login />} />
        <Route path="/about" element={<About />} />
        <Route path="*" element={<NoPage />} />
      </Routes>
      </div>
    </div>
  );
}

export default App;
