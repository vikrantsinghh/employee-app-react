import { combineReducers } from "redux";
import loginReducer from "./loginReducer";
import employeeReducer from "./employeeReducer";

const rootReducer = combineReducers(
    {
        user : loginReducer,
        storeEmployees : employeeReducer
    }
);

export default rootReducer;


