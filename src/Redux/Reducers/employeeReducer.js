export default function employeeReducer(state = [], action){
    switch(action.type){
        case "ADD_EMPLOYEES":
            return [...state, { ...action.employees}];
        default:
            return state;
    }
}